movies=# SELECT * FROM movies ORDER BY name;
 id |    name     | year 
----+-------------+------
  1 | El rey León | 1984
  3 | Star wars 1 |     
  2 | Terminator  |     
(3 rows)

movies=# SELECT * FROM movies ORDER BY name DESC;
 id |    name     | year 
----+-------------+------
  2 | Terminator  |     
  3 | Star wars 1 |     
  1 | El rey León | 1984
(3 rows)

movies=# SELECT * FROM movies ORDER BY name ASC;
 id |    name     | year 
----+-------------+------
  1 | El rey León | 1984
  3 | Star wars 1 |     
  2 | Terminator  |     
(3 rows)

movies=# SELECT * FROM movies ORDER BY name LIMIT 2;
 id |    name     | year 
----+-------------+------
  1 | El rey León | 1984
  3 | Star wars 1 |     
(2 rows)
