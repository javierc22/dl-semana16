# Semana 16

### Introducción a POSTGRESQL

1. Instalando PSQL en OSX
2. Instalación en Linux
3. SQL y PSQL
4. Usuarios y bases de datos
5. Usuarios y permisos
6. Administrando bases de datos
7. Administrando tablas
8. Autoincrementables, borrados e ingresos múltiples
9. SELECT y WHERE
10. Actualización y borrado
11. Ejercicio práctico 1
12. Queries con AND y OR
13. Búsqueda parcial
14. ORDER y LIMIT
15. COUNT y GROUP
16. HAVING
17. Ejercicio práctico 2
18. Restricciones y clave primaria

### POSTGRESQL avanzado

1. Introducción a múltiples tablas
2. SELECT de múltiples tablas
3. JOIN
4. COUNT y JOIN
5. Foreign key
6. Reflexiones sobre indices y constraints
