postgres=# \c desafiolatam
You are now connected to database "desafiolatam" as user "postgres".

desafiolatam=# CREATE TABLE tablamagica(
desafiolatam(# id SERIAL,
desafiolatam(# name VARCHAR(50)
desafiolatam(# );
CREATE TABLE

desafiolatam=# INSERT INTO tablamagica (name) VALUES ('prueba1');
INSERT 0 1
desafiolatam=# INSERT INTO tablamagica (name) VALUES ('prueba2');
INSERT 0 1
desafiolatam=# INSERT INTO tablamagica (name) VALUES ('prueba3');
INSERT 0 1

desafiolatam=# SELECT * FROM tablamagica
desafiolatam-# ;
  1 | prueba1
  2 | prueba2
  3 | prueba3

desafiolatam=# SELECT * FROM tablamagica;
  1 | prueba1
  2 | prueba2
  3 | prueba3

desafiolatam=# DELETE FROM tablamagica WHERE id = 3;
DELETE 1
desafiolatam=# SELECT * FROM tablamagica;
  1 | prueba1
  2 | prueba2

desafiolatam=# INSERT INTO tablamagica (name) VALUES ('prueba4'), ('prueba5'), ('prueba6');
INSERT 0 3
desafiolatam=# SELECT * FROM tablamagica;
  1 | prueba1
  2 | prueba2
  4 | prueba4
  5 | prueba5
  6 | prueba6
