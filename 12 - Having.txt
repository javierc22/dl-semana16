# Conectando a la Base de datos "desafiolatam":
movies=# \c desafiolatam
You are now connected to database "desafiolatam" as user "postgres".

# Creando tabla "products" con los campos "id", "price" y "category":
desafiolatam=# CREATE TABLE products (id SERIAL, price INTEGER, category VARCHAR(1));
CREATE TABLE
desafiolatam=# INSERT INTO products (price, category) VALUES (1000, 'A');
INSERT 0 1
desafiolatam=# INSERT INTO products (price, category) VALUES (1200, 'A');
INSERT 0 1
desafiolatam=# INSERT INTO products (price, category) VALUES (2200, 'A');
INSERT 0 1
desafiolatam=# INSERT INTO products (price, category) VALUES (1500, 'B');
INSERT 0 1
desafiolatam=# INSERT INTO products (price, category) VALUES (1600, 'B');
INSERT 0 1
desafiolatam=# INSERT INTO products (price, category) VALUES (1300, 'B');
INSERT 0 1

# Seleccionar todo desde la tabla "products":
desafiolatam=# SELECT * FROM products;
 id | price | category 
----+-------+----------
  1 |  1000 | A
  2 |  1200 | A
  3 |  2200 | A
  4 |  1500 | B
  5 |  1600 | B
  6 |  1300 | B
(6 rows)

# Contando y agrupando por categoría:
desafiolatam=# SELECT category, COUNT(*) FROM products GROUP BY category;
 category | count 
----------+-------
 B        |     3
 A        |     3
(2 rows)

# Agrupando por categoría y sumando precios:
desafiolatam=# SELECT category, SUM(price) FROM products GROUP BY category;
 category | sum  
----------+------
 B        | 4400
 A        | 4400
(2 rows)

# Agrupando por categoría, sumando precios de cada categoría y seleccionado cuya suma sea mayor a 3700:
desafiolatam=# SELECT category, SUM(price) FROM products GROUP BY category HAVING SUM(price) > 3700;
 category | sum  
----------+------
 B        | 4400
 A        | 4400
(2 rows)

# Agrupando por categoría y obteniendo promedio:
desafiolatam=# SELECT category, AVG(price) FROM products GROUP BY category;
 category |          avg          
----------+-----------------------
 B        | 1466.6666666666666667
 A        | 1466.6666666666666667
(2 rows)
