desafiolatam=# ALTER TABLE tablamagica ADD COLUMN age INTEGER;
ALTER TABLE
desafiolatam=# UPDATE tablamagica SET age = 28;
UPDATE 8

desafiolatam=# SELECT * FROM tablamagica:

  1 | prueba1 |  28
  2 | prueba2 |  28
  4 | prueba4 |  28
  5 | prueba5 |  28
  6 | prueba6 |  28
  7 | prueba4 |  28
  8 | prueba5 |  28
  9 | prueba6 |  28

desafiolatam=# UPDATE tablamagica SET age = 20 WHERE id = 1;
UPDATE 1
desafiolatam=# SELECT * FROM tablamagica;
  2 | prueba2 |  28
  4 | prueba4 |  28
  5 | prueba5 |  28
  6 | prueba6 |  28
  7 | prueba4 |  28
  8 | prueba5 |  28
  9 | prueba6 |  28
  1 | prueba1 |  20

desafiolatam=# UPDATE tablamagica SET age = 30 WHERE age = 28;
UPDATE 7
desafiolatam=# SELECT * FROM tablamagica;
  1 | prueba1 |  20
  2 | prueba2 |  30
  4 | prueba4 |  30
  5 | prueba5 |  30
  6 | prueba6 |  30
  7 | prueba4 |  30
  8 | prueba5 |  30
  9 | prueba6 |  30

desafiolatam=# DELETE FROM tablamagica WHERE id > 4;
DELETE 5
desafiolatam=# SELECT * FROM tablamagica;
  1 | prueba1 |  20
  2 | prueba2 |  30
  4 | prueba4 |  30
